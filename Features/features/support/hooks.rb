Before '@login' do

    $usuario_correto = 'supplier@phptravels.com'
    $senha_correta = 'demosupplier'
    $home = "Dashboard"
    $campo_obrigatorio = "Preencha este campo."
    $usuario_incorreto = Faker::Internet.email
    $senha_incorreta = Faker::Number.number(6) 
    $dados_incorretos = "Invalid Login Credentials"  
    $usuario_invalido = Faker::Name.name 
    @dados_invalidos = "The Email field must contain a valid email address"

end

After do
    Capybara.reset_sessions!
end

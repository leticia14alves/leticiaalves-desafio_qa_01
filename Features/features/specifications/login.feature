#language: pt
Funcionalidade: Realizar login

    Como usuario do sistema
    Quero poder realizar o login de acesso
    Para que consiga ter acesso as funcionalidades do sistema

Contexto: Acessar a tela de login

    Dado que estou na tela de login

@login_sucesso
Cenário: Realizar login com sucesso
    Quando eu realizo o login com sucesso
    Então vejo a mensagem informando o redirecionamento do usuario

@campos_obrigatorios
Cenário: Não informar campos obrigatorios
    Quando os campos obrigatorios nao sao informados
    Então a mensagem informando a necessidade desses campos e exibida

@usuario_nao_cadastrado
Cenário: Informar usuário não cadastrado no sistema
    Quando os dados informados nao estao cadastrados no sistema
    Então a mensagem informando que os dados informados nao estao cadastrados

@email_invalido
Cenário: Informar email inválido
    Quando o email informado e invalido
    Então a mensagem informando que o email e invalido e exibida



class Login
    include Capybara::DSL

    def initialize
        @login = LoginMapObjects.new
        visit "/supplier"
    end

    def preencher_dados_validos
        @login.txt_email.set $usuario_correto
        @login.txt_senha.set $senha_correta
    end

    def logar
        @login.btn_login.click
    end

    def redirecionamento_home
        expect(page).to have_selector('.fa.fa-home', exact_text: $home)
    end

    def mensagem_obrigatoriedade  
        expect(page).to have_selector('.toast-message', exact_text: $campo_obrigatorio)
    end

    def preencher_dados_nao_cadastrados
        @login.txt_email.set $usuario_incorreto
        @login.txt_senha.set $senha_incorreta
    end

    def mensagem_dados_incorretos  
        expect(page).to have_selector('.alert.alert-danger.loading.wow.fadeIn.animated.animated', exact_text: $dados_incorretos)
    end

    def preencher_email_invalido
        @login.txt_email.set $usuario_invalido
        @login.txt_senha.set $senha_incorreta
    end

    def mensagem_dados_invalidos  
        expect(page).to have_selector('.alert.alert-danger.loading.wow.fadeIn.animated.animated', exact_text: $dados_invalidos)
    end 
end
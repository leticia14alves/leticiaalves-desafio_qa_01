class LoginMapObjects < SitePrism::Page
    element :txt_email, '/html/body/div/form[1]/div[1]/input[1]'
    element :txt_Senha, '/html/body/div/form[1]/div[1]/input[2]'
    element :btn_login, '.btn.btn-primary.btn-block.ladda-button.fadeIn.animated'
end
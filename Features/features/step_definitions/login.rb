Dado("que estou na tela de login") do    
    visit "google.com"                                               
    @login = Login.new     
end                                                                                     
                                                                                          
Quando("eu realizo o login com sucesso") do                                             
    @login.preencher_dados_validos
    @login.logar          
 end                                                                                     
                                                                                          
Então("vejo a mensagem informando o redirecionamento do usuario") do                    
    @login.redirecionamento_home
end                                                                                     
                                                                                          
Quando("os campos obrigatorios nao sao informados") do                                  
    @login.logar          
end                                                                                     
                                                                                          
Então("a mensagem informando a necessidade desses campos e exibida") do                 
    @login.mensagem_obrigatoriedade          
end                                                                                     
                                                                                          
Quando("os dados informados nao estao cadastrados no sistema") do                       
    @login.preencher_dados_nao_cadastrados
    @login.logar
end                                                                                     
                                                                                          
Então("a mensagem informando que os dados informados nao estao cadastrados") do 
    @login.mensagem_dados_incorretos           
end                 

Quando("o email informado e invalido") do
    @login.preencher_email_invalido
    @login.logar
  end
  
  Então("a mensagem informando que o email e invalido e exibida") do
    @login.mensagem_dados_invalidos
  end
  
                                                                                          